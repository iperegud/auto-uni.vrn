package inheritance;

public class Entertainment {
    private  String name;

    public Entertainment(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void print (){
        System.out.println(name);
    }

    public static void main(String[] args) {
        Entertainment e = new Movie(2019, "movie");
        e.print ();
        e.setName("Drama");
        if (e instanceof Drama ) {
            Drama d = (Drama) e;
            d.print ();
        }
        else if(e instanceof Movie) {
            Movie m= (Movie)e;
            m.print();
        }
        else {
            System.out.println("incorrect type");
        }

      //  Movie m = e;
        // e.print();

        System.out.println(e instanceof Entertainment);
        System.out.println(e instanceof Movie);
    }
}
