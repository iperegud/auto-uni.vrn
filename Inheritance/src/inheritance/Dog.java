package inheritance;

public class Dog extends Animal{

    public Dog(String name, String address) {
        super (name, address);

    }
    @Override
    String saySomething() {
        return "woof";
    }
}

