package inheritance;

public class Abstractmain {
    public static void main(String[] args) {
        Animal [] setOfAnimals = new Animal[5];
        setOfAnimals [0] = new Cat ("Cat1", "Kirova, 11");
        setOfAnimals [1] = new Cat ("Cat2", "Mira, 5");
        setOfAnimals [2] = new Dog ("Dog1", "Kirova, 7");
        setOfAnimals [3] = new Dog ("Dog2", "Kirova, 5");
        setOfAnimals [4] = new Cat ("Cat3", "Mira, 5");
        for (int i=0; i <setOfAnimals.length; i++){
            System.out.println(setOfAnimals[i].getName() + " lives in " + setOfAnimals[i].getAddress());
        }

    }
}