package inheritance;

public abstract class Animal {
    private String name;
    private String address;
    public Animal (String name, String address){
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }
    public String getAddress() {
        return address;
    }
    abstract String saySomething();
}
