package reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class Main {
    public static void main(String[] args) throws ClassNotFoundException, NoSuchFieldException {

        Class person = Class.forName(Person.class.getName());
        System.out.println(Modifier.toString(person.getModifiers())+ " class "+ person.getSimpleName());

            Field[] fields = person.getDeclaredFields();
            for (Field field : fields) {

                System.out.println(Modifier.toString(field.getModifiers()) + " " + field.getType().getSimpleName() + " " + field.getName());
            }
            // getDeclaredFields() достает и приватные и публичные поля, судя по результату.
       /* Field[] fields2 = person.getFields();
        for (Field field : fields2) {

            System.out.println(Modifier.toString(field.getModifiers()) + " " + field.getType() + " " + field.getName());
        }*/


            Method [] methods =person.getDeclaredMethods();
            for (Method method: methods){
               /* Parameter [] parameters = method.getParameters();
                StringBuilder sb = new StringBuilder ();
                String param = null;
                for(Parameter parameter:parameters) {
                    param = sb.append(parameter.toString()).toString();
                }*/

                System.out.println(Modifier.toString(method.getModifiers()) + " " + method.getReturnType() + " " + method.getName()+ "(" + method.getParameters().toString()  ); //не смогла понять как использовать метод, который достает параметры, закомментировала кусок кода выше, тот же вопрос по пармаметрам в конструкторе
            }

            Constructor [] constructors = person.getConstructors();
            for (Constructor constructor:constructors){
                System.out.println(constructor.toString());
            }


    }
}
