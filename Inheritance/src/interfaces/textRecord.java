package interfaces;

public class textRecord extends abstractRecord implements Print{
    public textRecord(String author, String date, String message, MessageType messageType) {
        super(author, date, message, messageType);
    }

    @Override
    public void showMaxSize() {
        System.out.println("5");
    }

    @Override
    public void printOut() {
        System.out.println("This is a text.");
    }
}
