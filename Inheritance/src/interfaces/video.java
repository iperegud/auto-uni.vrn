package interfaces;

public class video extends mediaRecord implements Play {
    public video(String author, String date, String message, MessageType messageType) {
        super(author, date, message, messageType);
    }

    @Override
    public void play() {
        System.out.println("This is a video");
    }
}
