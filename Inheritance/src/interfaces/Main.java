package interfaces;

public class Main {
    public static void main(String[] args) {
        textRecord text = new textRecord("I. Ivanov", "10.10.2019", "Hello, World!", abstractRecord.MessageType.POST);
        System.out.println("The author of this " + text.getMessageType() + "is " +text.getAuthor()+". It was written on " + text.getDate() + " It says: "+ text.getMessage());
        System.out.print(" The max size is ");
        text.showMaxSize() ;
        text.printOut();

    }
}
