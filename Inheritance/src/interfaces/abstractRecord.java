package interfaces;

public abstract class abstractRecord {
        private String author;
        private String date;
        private String message;
        private MessageType messageType;

    public abstractRecord(String author, String date, String message, MessageType messageType) {
        this.author = author;
        this.date = date;
        this.message = message;
        this.messageType = messageType;
    }

    public String getAuthor() {
        return author;
    }

    public String getDate() {
        return date;
    }

    public String getMessage() {
        return message;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    enum MessageType{
            POST,
            REPOST,
            COMMENT
        }
        void showValues (abstractRecord a){
            System.out.println( "Author is "+a.getAuthor());
            System.out.println("Date is "+a.getDate());
            System.out.println("Message is "+a.getMessage());
            System.out.println("Message Type is "+a.getMessageType());
        }
        public abstract void showMaxSize();
        public static void main(String[] args) {

        }
}


