package interfaces;

public class Image extends mediaRecord implements Print{
    public Image(String author, String date, String message, MessageType messageType) {
        super(author, date, message, messageType);
    }

    @Override
    public void printOut() {
        System.out.println("This is a pic.");
    }
}
