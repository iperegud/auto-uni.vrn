package exceptions;

public class CanNotBeZeroException extends Exception{
    public CanNotBeZeroException(String message) {
        super(message);
    }
}
