package exceptions;

public class checkExceptions {
    public static void main(String[] args) {

        System.out.println(handleArg (2));
    }
    public static int checkArg (int a) throws CanNotBeZeroException{
        if (a ==0){
            throw new CanNotBeZeroException("Value equals zero");
        }
        return a;
    }
    public static int handleArg (int x) {
        try{
            return checkArg(x);
        }
        catch ( CanNotBeZeroException e){
            System.out.println(e.getMessage());
            throw new IllegalArgumentException(e.getMessage());
        }
    }
}
